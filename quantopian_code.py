#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 18:51:21 2018

@author: nunonobrega
"""

def initialize(context):
    context.security = symbol('TSLA')
    schedule_function(func = action_function,
                      date_rule = date_rules.every_day(),
                      time_rule = time_rules.market_open(hours = 0, minutes = 1)
                     
def action_function(context, data):
    price_history = data.history(asset = context.security,
                                 fields = 'price',
                                 bar_count = 10,
                                 frequency = '1d')
    
    average_price = price_history.mean()

    current_price = data.current(assets = context.security,
                                     fields = 'price'
                                 )
    if data.can_trade(context.security):                  
        if current_price > average_price * 1.03:
                      #We allocate entire portfolio here.
                      order_target_percent(context.security, 1)
        else:
                      #This will set position to 0.
                      order_target_percent(context.security, 0)
                      log.info('Setting my position to zero on {}'.format(context.security.symbol))
                      
print('tits')